# PartyGeek Scraper

Scraper application for SeatGeek.

## Requirements

* Python 2.x
* [pip](https://github.com/pypa/pip)

## Installation

* Clone this repository.
* After installing pip, run `pip install -r requirements.txt` in the root directory.
* Add a `settings.py` under the `boris` directory with the following contents:

~~~python
# -*- coding: utf-8 -*-
BOT_NAME = 'boris'

SPIDER_MODULES = ['boris.spiders']
NEWSPIDER_MODULE = 'boris.spiders'
ITEM_PIPELINES = {'boris.pipelines.BorisPipeline': 300}
DB_CONNECTION = 'mysql://user:password@localhost/partygeek'
GOOGLE_API_KEY = 'agoogleapikey'
~~~

## Running the Application

### Eventbrite Scraper

In the root directory, run `scrapy crawl eventbrite`.

### Joonbug Scraper

In the root directory, run `scrapy crawl joonbug`.
