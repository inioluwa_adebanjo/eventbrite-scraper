# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from boris.items import BorisItem

# Eventbrite spider
# This spider crawls Eventbrite's NYC nightlife event listings
class EventbriteSpider(scrapy.Spider):
    name = "eventbrite"
    allowed_domains = ["eventbrite.com"]
    start_urls = (
        'https://www.eventbrite.com/d/ny--new-york/nightlife/',
    )

    def parse(self, response):
        for sel in response.css('div.js-event-list-container > div.l-block-2'):
            item = BorisItem()

            title = sel.css('h4::text').extract()
            item['title'] = title[0].strip()

            item['url'] = sel.css('a::attr(href)').extract()

            details = sel.css('ul.bullet-list-ico > li')
            event_date = details[0].css('span::attr(content)').extract()[0]
            # Format the event's date into a MySQL DateTime format
            item['event_date'] = datetime.strptime(event_date, "%Y-%m-%dT%H:%M:%SZ").strftime("%Y-%m-%d %H:%M:%S")

            item['location'] = details[1].css('span > span > span[itemprop="address"] > meta[itemprop="name"]::attr(content)').extract()[0]
            item['source'] = self.name

            yield item
