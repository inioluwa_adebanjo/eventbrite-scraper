# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

# Event model
# Define the event table's columns and types
class Event(Base):
    __tablename__ = 'events'

    id = Column(Integer, primary_key=True)
    title = Column(String)
    url = Column(String)
    slug = Column(String)
    event_date = Column(String)
    location = Column(String)
    longitude = Column(Float)
    latitude = Column(Float)
    source = Column(String)
    crawled_at = Column(String)
    checksum = Column(String)

    def __repr__(self):
        return "<Event(title='%s')>" % (self.title)
